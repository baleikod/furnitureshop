from django.contrib import admin
from shop.models import Product, Tag, Category, Order, Review


class AdminProd(admin.ModelAdmin):
    list_display = ("admin_img", "name", "short_desc")


class AdminOrder(admin.ModelAdmin):
    list_display = ("product", "user", "created", "status", "phone")


class AdminReview(admin.ModelAdmin):
    list_display = ("product", "user", "created", "grade_item", "text")


admin.site.register(Tag)
admin.site.register(Category)
admin.site.register(Product, AdminProd)
admin.site.register(Order, AdminOrder)
admin.site.register(Review, AdminReview)
