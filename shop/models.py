import os
import uuid
from datetime import date
from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.dispatch.dispatcher import receiver
from django.utils.safestring import mark_safe
from django.db.models.signals import pre_save, post_delete

# from account.model import User


class Tag(models.Model):
    value = models.CharField(max_length=20)
    desc = models.TextField()

    def __str__(self):
        return f"#{self.value}"

    class Meta:
        db_table = "tags"


class Category(models.Model):
    name = models.CharField(max_length=15, verbose_name="Наименование")
    desc = models.TextField(verbose_name="Описание")
    alias = models.CharField(max_length=15, null=True, verbose_name="Алиас на англ")

    def __str__(self):
        return self.name

    class Meta:
        db_table = "category"
        verbose_name = "Категория"
        verbose_name_plural = "Категории"


class Product(models.Model):
    def image_path(self, filename):
        ext = filename.split(".")[-1]
        path = date.strftime(date.today(), "product/%Y/%m/%d")
        return f"{path}/{os.urandom(16).hex()}.{ext}"

    image = models.ImageField(upload_to=image_path)
    name = models.CharField(max_length=64, verbose_name="Наименование")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Создано")
    views = models.BigIntegerField(validators=[], default=0, verbose_name="Просмотры")
    short_desc = models.TextField(verbose_name="Короткое описание")
    text = models.TextField(verbose_name="Описание продукта")
    slug = models.SlugField(unique=True, blank=True)
    category = models.ForeignKey(
        Category, on_delete=models.CASCADE, related_name="category_prod", verbose_name="Категория"
    )
    tags = models.ManyToManyField(Tag, related_name="tag_prod", verbose_name="Тэги")
    price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name="Цена")

    class Meta:
        db_table = "products"
        verbose_name = "Продукт"
        verbose_name_plural = "Продукты"
        ordering = ("-created", "-id")

    def __str__(self):
        return self.name

    def admin_img(self):
        return mark_safe('<img src="/media/{}" width="200px" />'.format(self.image))

    admin_img.short_description = "Image"
    admin_img.allow_tags = True


# @receiver(pre_save, sender=Product)
# def resave_media(sender, instance, **kwargs):
#    if instance.id is not None


@receiver(post_delete, sender=Product)
def remove_media(sender, instance, **kwargs):
    try:
        path_to_file = settings.MEDIA_ROOT / str(instance.image.path)
        path_to_file.unlink()
    except (OSError, ValueError) as error:
        print(error)
    while True:
        try:
            path_to_file = (settings.MEDIA_ROOT / str(instance.image)).parent.absolute()
            #            path_to_file = (settings.MEDIA_ROOT / str(instance.image.path)).parent.absolute()
            path_to_file.rmdir()
        except (OSError, ValueError, UnboundLocalError) as error:
            break


@receiver(pre_save, sender=Product)
def set_slug(sender, instance, **kwargs):
    if instance.id is None:
        instance.slug = str(uuid.uuid4())


class Order(models.Model):
    STATUSES = [("N", "Новый"), ("С", "Подтвержден"), ("I", "Выполняется"), ("F", "Выполнен")]

    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name="prod_order", verbose_name="Проект")
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_order", verbose_name="Клиент")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Создан")
    status = models.CharField(max_length=1, choices=STATUSES, default="N", verbose_name="Статус заказа")
    phone = models.CharField(max_length=20, verbose_name="Телефон")

    class Meta:
        db_table = "orders"
        verbose_name = "Заказ"
        verbose_name_plural = "Заказы"
        ordering = ("-created", "-id")


class Review(models.Model):
    GRADE = [("1", "Ужасно"), ("2", "Плохо"), ("3", "Средне"), ("4", "Хорошо"), ("5", "Отлично")]

    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name="prod_review", verbose_name="Продукт")
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_review", verbose_name="Клиент")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Создано")
    grade_item = models.CharField(max_length=1, choices=GRADE, default="5", verbose_name="Оценка продукта")
    text = models.TextField(verbose_name="Отзыв")

    class Meta:
        db_table = "review"
        verbose_name = "Отзыв"
        verbose_name_plural = "Отзывы"
        ordering = ("-created", "-id")
