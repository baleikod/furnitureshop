from django.urls import path
from shop.views import ShopView, ProductView, OrderView, CategoryView, SearchResultsView

urlpatterns = [
    path("products/<slug:slug>/", ProductView.as_view(), name="product"),
    path("order/<slug:slug>/", OrderView.as_view(), name="order"),
    path("category/", CategoryView.as_view(), name="category"),
    path("search/", SearchResultsView.as_view(), name="search_results"),
    path("", ShopView.as_view(), name="catalogs_page"),
]
