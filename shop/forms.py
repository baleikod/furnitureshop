from django import forms
from shop.models import Review, Order


class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review
        fields = ("grade_item", "text")
        grade = (("1", "Ужасно"), ("2", "Плохо"), ("3", "Средне"), ("4", "Хорошо"), ("5", "Отлично"))
        widgets = {
            "grade_item": forms.RadioSelect(attrs={"class": "dropdown-item", "id": "choice"}, choices=grade),
            "text": forms.Textarea(attrs={"class": "massage-bt", "rows": "3", "id": "text", "placeholder": "Текст"}),
        }


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ("phone",)
        widgets = {"phone": forms.TextInput(attrs={"class": "mail_text", "id": "phone", "placeholder": "Телефон"})}

    def clean(self):
        has_errors = False
        if not self.cleaned_data["phone"].isnumeric():
            has_errors = True

        if len(self.cleaned_data["phone"]) < 6:
            has_errors = True

        if has_errors:
            raise forms.ValidationError("Invalid form")
        return self.cleaned_data
