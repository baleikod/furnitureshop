from django.db.models import Q
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.views.generic import ListView
from django.views.generic.edit import FormMixin
from shop.forms import ReviewForm, OrderForm
from django.views.generic.detail import DetailView
from shop.models import Product, Category
from django.contrib import messages


class ShopView(ListView):
    template_name = "catalog.html"
    model = Product
    paginate_by = 4

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(ShopView, self).get_context_data(**kwargs)
        context["categorys"] = Category.objects.all()
        return context


class SearchResultsView(ListView):
    template_name = "catalog_filter.html"
    model = Product
    paginate_by = 4

    def urlencode_filter(self):
        qd = self.request.GET.copy()
        qd.pop(self.page_kwarg, None)
        return qd.urlencode()

    def get_queryset(self):
        query = self.request.GET.get("search")
        object_list = Product.objects.filter(Q(name__iregex=query) | Q(text__iregex=query))
        return object_list

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(SearchResultsView, self).get_context_data(**kwargs)
        context["categorys"] = Category.objects.all()
        return context


class CategoryView(ListView):
    template_name = "catalog_filter.html"
    model = Product  # Category
    paginate_by = 4

    def urlencode_filter(self):
        qd = self.request.GET.copy()
        qd.pop(self.page_kwarg, None)
        return qd.urlencode()

    def get_queryset(self):
        query = self.request.GET.get("category")
        object_list = Product.objects.filter(category__alias=query)
        return object_list

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(CategoryView, self).get_context_data(**kwargs)
        context["categorys"] = Category.objects.all()
        return context


class ProductView(FormMixin, DetailView):
    template_name = "product.html"
    model = Product
    form_class = ReviewForm

    def form_valid(self, form):
        product = get_object_or_404(Product, slug=self.kwargs.get("slug"))
        review = form.save(commit=False)
        review.product = product
        review.user = self.request.user
        form.save()
        return super(ProductView, self).form_valid(form)

    def get_success_url(self):
        return reverse("product", kwargs={"slug": self.object.slug})

    def get_context_data(self, **kwargs):
        context = super(ProductView, self).get_context_data(**kwargs)
        context["form"] = ReviewForm(initial={"post": self.object})
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class OrderView(FormMixin, DetailView):
    template_name = "order.html"
    model = Product
    form_class = OrderForm

    def form_valid(self, form):
        product = get_object_or_404(Product, slug=self.kwargs.get("slug"))
        review = form.save(commit=False)
        review.product = product
        review.user = self.request.user
        form.save()
        return super(OrderView, self).form_valid(form)

    def get_success_url(self):
        messages.info(self.request, "Заявка принята, наш менеджер свяжется с вами!")
        return reverse("catalogs_page")

    def get_context_data(self, **kwargs):
        context = super(OrderView, self).get_context_data(**kwargs)
        context["form"] = OrderForm(initial={"post": self.object})
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            messages.info(self.request, "Заявка отклонена. Заполните номер телефона правильно!")
            return self.form_invalid(form)
