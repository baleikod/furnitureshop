from django.contrib import admin
from django.urls import path, include
from django.shortcuts import redirect
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path("admin/", admin.site.urls),
    path("account/registration/", include("account.urls")),
    path("account/", include("django.contrib.auth.urls")),
    path("home/", include("home.urls")),
    path("shop/", include("shop.urls")),
    path("", lambda request: redirect("home_page")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
