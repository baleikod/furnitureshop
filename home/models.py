from django.db import models


class Appeal(models.Model):
    STATUSES = [("N", "Новое"), ("I", "В обработке"), ("W", "Обработано")]

    username = models.CharField(max_length=30, verbose_name="Полное имя")
    email = models.EmailField(verbose_name="Электронная почта")
    company = models.CharField(max_length=20, verbose_name="Место работы")
    message = models.TextField(verbose_name="Сообщение")
    status = models.CharField(max_length=1, choices=STATUSES, default="N", verbose_name="Статус обращения")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Создано")
    updated = models.DateTimeField(auto_now=True, verbose_name="Обновлено")

    class Meta:
        db_table = "appeal"
        verbose_name = "Обращение"
        verbose_name_plural = "Обращения"

    def __str__(self):
        return self.username
