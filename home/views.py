from django.shortcuts import render, redirect
from django.contrib import messages
from home.forms import AppealForm
from shop.models import Product


def home_page(request):
    product_list1 = Product.objects.all()[:3]
    product_list2 = Product.objects.all()[3:6]
    return render(
        request, "home.html", {"product_list1": product_list1, "product_list2": product_list2, "page_tag": "home"}
    )


def about(request):
    return render(request, "about.html", {"page_name": "About Pod", "page_tag": "about"})


def shop(request):
    return render(request, "search.html", {"page_name": "Shop_page", "page_tag": "shop"})


def contact(request):
    aform = AppealForm()
    if request.method == "POST":
        aform = AppealForm(request.POST)
        if aform.is_valid():
            aform.save()
            messages.info(request, "Обращение добавлено.")
            return redirect("contact_page")
    return render(request, "contact.html", {"aform": aform, "page_name": "Contact Page", "page_tag": "contact"})
