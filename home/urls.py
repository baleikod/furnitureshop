from django.urls import path

from home.views import home_page, about, shop, contact


urlpatterns = [
    path("", home_page, name="home_page"),
    path("about/", about, name="about_page"),
    path("search/", shop, name="search_page"),
    path("contact/", contact, name="contact_page"),
]
