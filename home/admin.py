from django.contrib import admin
from home.models import Appeal


class AdminAppeal(admin.ModelAdmin):
    list_display = ("username", "email", "status", "company", "message", "updated")


admin.site.register(Appeal, AdminAppeal)
