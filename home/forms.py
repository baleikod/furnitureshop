from django import forms
from home.models import Appeal


class AppealForm(forms.Form):
    username = forms.CharField(
        label="", widget=forms.TextInput(attrs={"class": "mail_text", "id": "full-name", "placeholder": "Полное имя"})
    )
    email = forms.EmailField(
        label="",
        widget=forms.EmailInput(
            attrs={"class": "mail_text", "id": "email", "placeholder": "Email", "pattern": "[^ @]*@[^ @]*"}
        ),
    )
    company = forms.CharField(
        label="", widget=forms.TextInput(attrs={"class": "mail_text", "id": "name", "placeholder": "Место работы"})
    )

    message = forms.CharField(
        label="",
        widget=forms.Textarea(
            attrs={"class": "massage-bt", "rows": "5", "id": "comment", "placeholder": "Введите сообщение тут"}
        ),
    )

    def clean(self):
        has_errors = False
        if len(self.cleaned_data["message"]) > 250:
            self.add_error("message", "A lot of size")
            has_errors = True

        if has_errors:
            raise forms.ValidationError("Invalid form")
        return self.cleaned_data

    def save(self):
        Appeal.objects.create(**self.cleaned_data)
