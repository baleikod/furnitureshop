from django import forms
from account.models import User


class RegistrationForm(forms.ModelForm):
    password_repeat = forms.CharField(
        widget=forms.PasswordInput(
            attrs={"class": "mail_text", "id": "password_repeate", "placeholder": "Повтор пароля"}
        )
    )

    class Meta:
        model = User
        fields = ("username", "email", "password")
        widgets = {
            "username": forms.TextInput(attrs={"class": "mail_text", "id": "username", "placeholder": "Пользователь"}),
            "email": forms.EmailInput(attrs={"class": "mail_text", "id": "email", "placeholder": "email"}),
            "password": forms.PasswordInput(attrs={"class": "mail_text", "id": "password", "placeholder": "Пароль"}),
        }

    def clean(self):
        has_errors = False
        password = self.cleaned_data["password"]
        password_repeat = self.cleaned_data["password_repeat"]
        if password != password_repeat:
            self.add_error("password_repeat", "Пароли не совпадают")
            has_errors = True

        if has_errors:
            raise forms.ValidationError("Invalid form")
        return self.cleaned_data
